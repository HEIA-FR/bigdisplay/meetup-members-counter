module gitlab.com/heia-fr/bigdisplay/meetup-members-counter

require (
	github.com/eclipse/paho.mqtt.golang v1.1.1
	github.com/sirupsen/logrus v1.1.1
	golang.org/x/net v0.0.0-20181023162649-9b4f9f5ad519 // indirect
)
